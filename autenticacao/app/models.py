from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nome = models.CharField(max_length=50, blank=True, null=True)
    exper = models.TextField(max_length=1000, blank=True, null=True)
    curreal = models.TextField(max_length=1000, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    telefone = models.CharField(max_length=12, blank=True, null=True)
    curso = models.CharField(max_length=20, blank=True, null=True)
    periodo = models.CharField(max_length=2, blank=True, null=True)
    funcao = models.CharField(max_length=20, blank=True, null=True)
    habilidades = models.CharField(max_length=100, blank=True, null=True)
    disponibilidade = models.BooleanField(default=True)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()